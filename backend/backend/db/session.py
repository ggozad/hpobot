import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

POSTGRES_USER = os.environ['POSTGRES_USER']
POSTGRES_PASSWORD = os.environ['POSTGRES_PASSWORD']
POSTGRES_HOST = os.environ['POSTGRES_HOST']
POSTGRES_DB = os.environ['POSTGRES_DB']

ASYNC_DATABASE_URL = f"postgresql+asyncpg://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:5432/{POSTGRES_DB}"
SYNC_DATABASE_URL = ASYNC_DATABASE_URL.replace("+asyncpg", "")

# Uncomment the following lines to use the async engine
# from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
# engine = create_async_engine(
#     ASYNC_DATABASE_URL,
#     echo=False,
# )
# async_session = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)

engine = create_engine(SYNC_DATABASE_URL, pool_pre_ping=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
