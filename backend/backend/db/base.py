# Import all the models, so that Base has them before being imported by Alembic

from backend.db.base_class import Base  # noqa: F401
from backend.db.models.user import User  # noqa: F401
