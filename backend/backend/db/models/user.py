from backend.db.base_class import Base
from sqlalchemy import Column, String


class User(Base):
    __tablename__ = "users"

    email = Column(String, nullable=False, unique=True, index=True)
    fullname = Column(String, nullable=True)
    hashed_password = Column(String, nullable=False)
