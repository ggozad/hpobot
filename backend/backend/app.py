from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from backend.api import api_router

app = FastAPI()
origins = [
    "https://api.local.hpobot.dev",
    "https://api.local.hpobot.dev:8080",
    "https://app.local.hpobot.dev",
    "https://app.local.hpobot.dev:8080",
    "https://localhost",
    "https://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(api_router, prefix="/api/v1")
