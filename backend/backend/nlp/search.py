import logging as log
import os

import nltk
import torch
from backend.hpo.ontology import FeatureExtractor, ontology
from backend.nlp.biobert import BIOBERT_MODEL_ID
from backend.utils.files import clean_filename
from backend.utils.singleton import Singleton
from sentence_transformers import SentenceTransformer, util

MSMARCO_MODEL_ID = "sentence-transformers/msmarco-distilbert-base-v4"
DATA_DIR = os.environ.get("DATA_DIR")
NLTK_DATA = os.environ.get("NLTK_DATA")
TOKENIZER_ID = "tokenizers/punkt/english.pickle"


class SemanticSearch(metaclass=Singleton):
    def __init__(self, model_id=MSMARCO_MODEL_ID) -> None:
        self.embedder = SentenceTransformer(model_id)
        self.feature_extractor = FeatureExtractor(ontology)
        self.corpus = self.feature_extractor.to_corpus()
        self.reverse_corpus = {}
        for term, sentences in self.corpus.items():
            for sentence in sentences:
                self.reverse_corpus[sentence] = term
        corpus = [k for k in self.reverse_corpus.keys()]
        self.sentence_corpus = corpus
        path = os.path.join(
            DATA_DIR, clean_filename("%s-corpus_embeddings.pt" % model_id)
        )

        if os.path.isfile(path):
            log.info("Loading embeddings from %s", path)
            self.embeddings = torch.load(path)
        else:
            log.info("Computing sentence embeddings, this will take a while...")
            self.embeddings = self.embedder.encode(corpus, convert_to_tensor=True)
            torch.save(self.embeddings, path)
            log.info("Computing sentence embeddings complete")

        path = os.path.join(DATA_DIR, NLTK_DATA, TOKENIZER_ID)
        if not os.path.isfile(path):
            log.info("Downloading NLTK tokenizers")
            nltk.download("punkt", download_dir=os.path.join(DATA_DIR, NLTK_DATA))
        self.tokenizer = nltk.data.load(TOKENIZER_ID)

    def sentence_search(self, sentence: str, top_k: int = 5) -> str:
        embedding = self.embedder.encode(sentence, convert_to_tensor=True)
        scores = util.pytorch_cos_sim(embedding, self.embeddings)[0]
        top_results = torch.topk(scores, k=top_k)

        return {
            self.reverse_corpus[self.sentence_corpus[idx]]: score.item()
            for score, idx in zip(top_results[0], top_results[1])
        }

    def search(self, text: str, top_k: int = 5) -> str:
        sentences = self.tokenizer.tokenize(text)
        results = {}
        for sentence in sentences:
            result = self.sentence_search(sentence, top_k=top_k)
            results.update(result)
        return results


# Initialize SemanticSearch when the module is loaded to speed up things.
# SemanticSearch()
