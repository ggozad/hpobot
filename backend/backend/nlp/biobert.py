# from transformers import AutoTokenizer, AutoModel
from sentence_transformers import SentenceTransformer

BIOBERT_MODEL_ID = "dmis-lab/biobert-v1.1"

# tokenizer = AutoTokenizer.from_pretrained(BIOBERT_MODEL_ID)
# model = AutoModel.from_pretrained(BIOBERT_MODEL_ID)
embedder = SentenceTransformer(BIOBERT_MODEL_ID)
