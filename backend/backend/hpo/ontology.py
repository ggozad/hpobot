import logging as log
import os
from urllib.request import urlretrieve

import pronto

DATA_DIR = os.environ.get("DATA_DIR")
HP_OBO_URL = "https://raw.githubusercontent.com/obophenotype/human-phenotype-ontology/master/hp.obo"


class FeatureExtractor:
    def __init__(self, ontology):
        self.ontology = ontology

    def to_json(
        self, rootId: str = None, with_children: bool = True, with_parent: bool = False
    ):
        try:
            if rootId is None:
                root = self.ontology.get_term("HP:0000001")
            else:
                root = self.ontology.get_term(rootId)
            if with_parent:
                root = [t for t in root.superclasses(1)][-1]

        except KeyError:
            return None

        json = {
            "id": root.id,
            "name": root.name,
            "definition": root.definition,
            "comment": root.comment,
            "synonyms": [s.description for s in root.synonyms],
        }
        if with_children:
            json["subclasses"] = [
                self.to_json(rootId=subclass.id, with_children=with_children)
                for subclass in root.subclasses(distance=1, with_self=False)
                if not with_parent or subclass.id == rootId
            ]

        return json

    def to_corpus(self, root: str = None):
        if root is None:
            root = self.ontology.get_term("HP:0000001")
        else:
            root = self.ontology.get_term(root)

        terms = root.subclasses(with_self=True)

        corpus = {
            term.id: [
                term.name,
                term.definition.title(),
                *[synonym.description for synonym in term.synonyms],
            ]
            for term in terms
            if term.definition is not None
        }
        return corpus


hpo_file = os.path.join(DATA_DIR, "hp.obo")
if not os.path.exists(hpo_file):
    log.info('Downloading HPO ontology from "%s"', HP_OBO_URL)
    tmp_file = os.path.join(DATA_DIR, "hp.obo.original")
    urlretrieve(HP_OBO_URL, tmp_file)
    with open(tmp_file, "r") as hpo_in, open(
        hpo_file, "w", encoding="utf-8"
    ) as hpo_out:
        for line in hpo_in:
            line.replace("EXACT layperson []", "EXACT []")
            hpo_out.write(line + "\n")

log.info('Found HPO ontology at "%s"', hpo_file)

ontology = pronto.Ontology(hpo_file)
extractor = FeatureExtractor(ontology)
