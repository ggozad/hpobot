from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter()


class TestSchema(BaseModel):
    q: str


@router.post("/", response_model=str)
def test(query: TestSchema):
    return query.q
