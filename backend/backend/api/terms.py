from backend.api import deps
from backend.db import models
from backend.hpo.ontology import extractor
from fastapi import APIRouter, Depends
from pydantic import BaseModel

router = APIRouter()


class TermQuery(BaseModel):
    id: str
    includeChildren: bool = True


@router.post("/")
async def term(
    query: TermQuery, current_user: models.User = Depends(deps.get_current_user)
):
    return extractor.to_json(
        rootId=query.id, with_children=query.includeChildren, with_parent=True
    )
