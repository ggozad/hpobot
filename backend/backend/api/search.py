from backend.api import deps
from backend.db import models
from backend.nlp.search import SemanticSearch
from fastapi import APIRouter, Depends
from pydantic import BaseModel

router = APIRouter()


class SearchQuery(BaseModel):
    text: str


@router.post("/")
async def terms(
    query: SearchQuery, current_user: models.User = Depends(deps.get_current_user)
):
    ss = SemanticSearch()
    return ss.search(query.text)
