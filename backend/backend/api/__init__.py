from backend.api import login, search, terms, test, users
from fastapi import APIRouter

api_router = APIRouter()
api_router.include_router(login.router, tags=["login"])
api_router.include_router(test.router, prefix="/test", tags=["test"])
api_router.include_router(terms.router, prefix="/terms", tags=["terms"])
api_router.include_router(search.router, prefix="/search", tags=["search"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
