import { HPO, HPOMetaCollection, HPOTreeNode } from 'types/hpo'

export const HPOToTree = (hpo: HPO): HPOTreeNode => {
  return {
    name: hpo.name,
    id: hpo.id,
    definition: hpo.definition,
    synonyms: hpo.synonyms,
    children: hpo.subclasses?.map(HPOToTree),
  }
}

export const HPOToReaflowTree = (hpo: HPO, expanded: string[] = []) => {
  let flatnodes = [
    {
      id: hpo.id,
      text: hpo.name,
      height: 80,
      data: {
        definition: hpo.definition,
        hasChildren: hpo.subclasses && hpo.subclasses?.length > 0,
      },
    },
  ]
  let flatedges: any[] = []
  if (expanded.includes(hpo.id)) {
    flatedges =
      hpo.subclasses?.map((subclass) => {
        return {
          id: `${hpo.id}-${subclass.id}`,
          from: hpo.id,
          to: subclass.id,
        }
      }) || []
    hpo.subclasses?.forEach((subclass) => {
      const { nodes, edges } = HPOToReaflowTree(subclass, expanded)
      flatedges = [...flatedges, ...edges]
      flatnodes = [...flatnodes, ...nodes]
    })
  }
  flatnodes = [...new Map(flatnodes.map((item) => [item.id, item])).values()]
  return { nodes: flatnodes, edges: flatedges }
}

// extracts all HPO definitions, synonyms from an HPO tree
export const extractHPOMeta = (hpo: HPO): HPOMetaCollection => {
  let definitions: HPOMetaCollection = {}
  definitions[hpo.id] = { definition: hpo.definition, synonyms: hpo.synonyms }

  if (hpo.subclasses) {
    hpo.subclasses.forEach((subclass) => {
      definitions = { ...definitions, ...extractHPOMeta(subclass) }
    })
  }

  return definitions
}
