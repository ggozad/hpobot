import { combineReducers, configureStore } from '@reduxjs/toolkit'

import userReducer from 'store/user/usersSlice'

const CLEAR_STORE = 'CLEAR_STORE'

const reducer = combineReducers({
  user: userReducer,
})

const rootReducer = (state, action) => {
  if (action.type === CLEAR_STORE) {
    return reducer(undefined, action)
  }
  return reducer(state, action)
}
export const store = configureStore({
  reducer: rootReducer,
})

// Resets the store to its initial state
export const clearStore = { type: CLEAR_STORE }
export type RootState = ReturnType<typeof reducer>
