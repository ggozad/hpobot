import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import API from 'API'

import type { RootState } from 'store/store'

import { User } from 'types/User'

const initialState: User = { id: '', email: '', fullname: '' }

export const fetchCurrentUser = createAsyncThunk<number>(
  'user/fetchCurrentUser',
  async () => {
    const user = await API.getCurrentUser()
    return user
  },
)

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchCurrentUser.fulfilled, (state, action) =>
      Object.assign(state, action.payload),
    )
  },
})

export const selectCurrentUser = (state: RootState) => {
  if (!state.user.id) {
    return undefined
  }
  return state.user
}

export const selectCurrentUserFullName = (state: RootState) =>
  selectCurrentUser(state)?.fullname

export default userSlice.reducer
