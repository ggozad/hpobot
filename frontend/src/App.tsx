import React from 'react'
import { Outlet } from 'react-router-dom'

import { HPOProvider } from 'components/HPOContext'
import TopNavBar from 'components/TopNavBar'

function App() {
  return (
    <HPOProvider>
      <div className="w-full h-screen">
        <TopNavBar />
        <Outlet />
      </div>
    </HPOProvider>
  )
}

export default App
