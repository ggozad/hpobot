export interface HPO {
  id: string
  name: string
  definition: string
  synonyms: string[]
  subclasses?: HPO[]
}

export interface HPOTreeNode {
  id: string
  name: string
  definition: string
  synonyms: string[]
  children?: HPOTreeNode[]
  parent?: HPOTreeNode
}

export interface HPOMeta {
  definition?: string
  synonyms?: string[]
}

export interface HPOMetaCollection {
  [key: string]: HPOMeta
}
