import API from 'API'
import * as React from 'react'
import { Context, ReactElement, ReactNode } from 'react'
import { useDispatch } from 'react-redux'

import { fetchCurrentUser } from 'store/user/usersSlice'

const authContext = React.createContext<Partial<AuthContext>>({})

interface AuthContext {
  authenticated: boolean
  login: (username?: string, password?: string) => Promise<void>
  logout: () => void
}

// A custom hook that exposes the authentication state, a login() and a logout() function.
function useAuth(): AuthContext {
  const [authenticated, setAuthenticated] = React.useState(false)
  const dispatch = useDispatch()

  const postAuth = () => {
    const promises = [dispatch(fetchCurrentUser())]
    // Fetch what's necessary post auth here
    return Promise.all(promises).then(() => setAuthenticated(true))
  }
  const login = async (username, password): Promise<void> => {
    if (username === undefined || password === undefined) {
      const token = sessionStorage.getItem('authToken')
      if (token) {
        await API.logInTestToken(token)
        return postAuth()
      }
      return Promise.reject('Could not authenticate')
    }
    const token = await API.logInGetToken(username, password)
    sessionStorage.setItem('authToken', token.access_token)
    return postAuth()
  }
  const logout = () => {
    sessionStorage.removeItem('authToken')
    setAuthenticated(false)
  }

  return {
    authenticated,
    login,
    logout,
  }
}

export function AuthProvider({ children }: { children: ReactNode }): ReactElement {
  const auth = useAuth()
  return <authContext.Provider value={auth}>{children}</authContext.Provider>
}

export default function AuthConsumer(): AuthContext {
  return React.useContext(authContext as Context<AuthContext>)
}
