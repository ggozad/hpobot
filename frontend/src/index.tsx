import App from './App'
import './index.css'
import reportWebVitals from './reportWebVitals'
import HPOExplorer from './views/HPOExplorer'
import Login from './views/Login'
import useAuth, { AuthProvider } from 'auth'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter, Navigate, Outlet, Route, Routes } from 'react-router-dom'

import { store } from 'store/store'

import HPOSearch from 'views/HPOSearch'

function AuthenticatedRoute() {
  const { authenticated } = useAuth()
  return authenticated ? <Outlet /> : <Navigate to={{ pathname: '/login' }} />
}

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <AuthProvider>
          <Routes>
            <Route path="/" element={<App />}>
              <Route element={<AuthenticatedRoute />}>
                <Route index element={<HPOSearch />} />
              </Route>
              <Route element={<AuthenticatedRoute />}>
                <Route path="explore" element={<HPOExplorer />} />
              </Route>
              <Route path="login" element={<Login />} />
            </Route>
          </Routes>
        </AuthProvider>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)

reportWebVitals()
