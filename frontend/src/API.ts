import { HPO } from './types/hpo'
import axios from 'axios'

axios.defaults.withCredentials = true
axios.defaults.timeout = 15_000
axios.defaults.baseURL = process.env.REACT_APP_BASE_API_URL

function authHeaders() {
  const token = sessionStorage.getItem('authToken')

  return {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
}

namespace API {
  /* Terms */

  export const logInGetToken = async (username: string, password: string) => {
    const path = '/api/v1/login/access-token'
    const params = new URLSearchParams()
    params.append('username', username)
    params.append('password', password)

    return axios.post(path, params).then((response) => response.data)
  }

  export const logInTestToken = async (token: string) => {
    const path = '/api/v1/login/test-token'
    return axios.post(path, {}, authHeaders()).then((response) => response.data)
  }

  export const getCurrentUser = async () => {
    const path = '/api/v1/users/me'
    return axios.get(path, authHeaders()).then((response) => response.data)
  }

  export const getTerm = (term: string) => {
    const path = `/api/v1/terms`
    return axios
      .post<HPO>(path, { id: term }, authHeaders())
      .then((response) => response.data)
  }

  type SearchResponse = Record<string, number>

  export const searchPhenotype = (text: string) => {
    const path = `/api/v1/search`
    return axios
      .post<SearchResponse>(path, { text }, authHeaders())
      .then((response) => response.data)
  }
}

export default API
