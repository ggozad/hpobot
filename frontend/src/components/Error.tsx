import React from 'react'

interface Props {
  error: string
}

export default function Error({ error }: Props) {
  if (!error) return null
  return <div className="text-center text-sm text-red-500 p-1">{error}</div>
}
