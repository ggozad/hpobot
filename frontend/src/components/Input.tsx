import React, { InputHTMLAttributes } from 'react'

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  label?: string
  error?: string
}

export default function Input({ label, error, ...props }: Props) {
  return (
    <div className="flex flex-col w-full">
      {error && <div className="text-sm text-red-500 p-1">{error}</div>}

      <div className="flex w-full">
        {label && (
          <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">
            {label}
          </span>
        )}
        <input
          className="flex-1 block w-full focus:ring-indigo-500 focus:border-indigo-500 min-w-0 disabled:text-red-100 rounded-none rounded-r-md sm:text-sm border-gray-300 border p-2"
          {...props}
        />
      </div>
    </div>
  )
}
