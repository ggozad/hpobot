import React, { TextareaHTMLAttributes } from 'react'

interface Props extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  label?: string
  rows?: number
}

export default function TextArea({ label, rows, ...props }: Props) {
  return (
    <div className="flex flex-col w-full">
      <div className="flex w-full">
        {label && (
          <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">
            {label}
          </span>
        )}
        <textarea
          rows={rows || 4}
          className="flex-1 block w-full focus:ring-indigo-500 focus:border-indigo-500 min-w-0 disabled:text-red-100 rounded-none rounded-r-md sm:text-sm border-gray-300 border p-2"
          {...props}
        />
      </div>
    </div>
  )
}
