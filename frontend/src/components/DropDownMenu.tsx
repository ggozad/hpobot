import { Menu as HeadlessUiMenu, Transition } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/solid'
import React, { FC, Fragment, ReactNode } from 'react'

interface Props {
  title: string
  children: ReactNode
}

function Menu({ title, children }: Props) {
  return (
    <HeadlessUiMenu as="div" className="mh-3 relative">
      <div className="h-full w-full">
        <HeadlessUiMenu.Button className="bg-white flex justify-center items-center w-full h-full justify-stretch rounded-md px-2 text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
          {title}
          <ChevronDownIcon
            className="w-5 h-5 ml-2 -mr-1 text-violet-200 hover:text-violet-100"
            aria-hidden="true"
          />
        </HeadlessUiMenu.Button>
      </div>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-200"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <HeadlessUiMenu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none z-10">
          {children}
        </HeadlessUiMenu.Items>
      </Transition>
    </HeadlessUiMenu>
  )
}

interface MenuItemProps {
  onClick?
  title
  LeftIcon?: FC<any>
}

function MenuItem({ onClick, title, LeftIcon }: MenuItemProps) {
  return (
    <HeadlessUiMenu.Item>
      {({ active }) => (
        <button
          className={`${
            active ? 'bg-gray-100' : ''
          } group flex rounded-md items-center w-full px-2 py-2 text-sm text-gray-700`}
          onClick={onClick}
          type="button"
        >
          {LeftIcon && <LeftIcon className="w-5 h-5 mr-2" aria-hidden="true" />}
          {title}
        </button>
      )}
    </HeadlessUiMenu.Item>
  )
}

Menu.MenuItem = MenuItem
export default Menu
