import React, { Context, ReactElement, ReactNode, useState } from 'react'

interface ReturnType {
  included: Set<string>
  excluded: Set<string>
  include: (id: string) => void
  exclude: (id: string) => void
}

const hpoContext = React.createContext<Partial<ReturnType>>({})

function useHPO(): ReturnType {
  const [included, setIncluded] = useState<Set<string>>(new Set([]))
  const [excluded, setExcluded] = React.useState<Set<string>>(new Set([]))
  return {
    included,
    excluded,
    include: (id) => {
      if (!included.has(id)) {
        setIncluded(new Set([...included, id]))
        if (excluded.has(id)) {
          setExcluded(new Set([...excluded].filter((x) => x !== id)))
        }
      } else {
        setIncluded(new Set([...included].filter((x) => x !== id)))
      }
    },
    exclude: (id) => {
      if (!excluded.has(id)) {
        setExcluded(new Set([...excluded, id]))
        if (included.has(id)) {
          setIncluded(new Set([...included].filter((x) => x !== id)))
        }
      } else {
        setExcluded(new Set([...excluded].filter((x) => x !== id)))
      }
    },
  }
}

export function HPOProvider({ children }: { children: ReactNode }): ReactElement {
  const hpo = useHPO()
  return <hpoContext.Provider value={hpo}>{children}</hpoContext.Provider>
}

export default function HPOConsumer(): ReturnType {
  return React.useContext(hpoContext as Context<ReturnType>)
}
