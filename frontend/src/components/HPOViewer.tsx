import {
  ChevronDoubleLeftIcon,
  ChevronDoubleRightIcon,
  MinusCircleIcon,
  PlusCircleIcon,
} from '@heroicons/react/solid'
import API from 'API'
import React, { useEffect, useState } from 'react'
import { Canvas, CanvasContainerProps, Node, NodeProps } from 'reaflow'

import useHPO from 'components/HPOContext'

import { HPO } from 'types/hpo'

import { HPOToReaflowTree } from 'utils/hpo'

interface HPOViewerProps {
  term: string
  onSelect?: ({
    included,
    excluded,
  }: {
    included: Set<string>
    excluded: Set<string>
  }) => void
}

export default function HPOViewer({ term, onSelect }: HPOViewerProps) {
  const { include, exclude } = useHPO()

  const [root, setRoot] = useState<HPO | null>(null)
  const [expanded, setExpanded] = useState<string[]>([])
  const [treeData, setTreeData] = useState<CanvasContainerProps | null>(null)
  const [activeNode, setActiveNode] = useState<string | null>(null)
  const [definition, setDefinition] = useState<string | null>('')
  const [bubblePosition, setBubblePosition] = useState({ x: 0, y: 0 })

  useEffect(() => {
    API.getTerm(term).then((term) => {
      if (term) {
        setRoot(term)
      }
    })
  }, [term])

  useEffect(() => {
    if (root) {
      setTreeData(HPOToReaflowTree(root as HPO, expanded))
    }
  }, [root, expanded])

  useEffect(() => {
    if (definition) {
      let timer = setTimeout(() => {
        setDefinition('')
      }, 5000)
      return () => clearTimeout(timer)
    }
  }, [definition])

  const onNodeEnter = (ev, node) => {
    const bRect = ev.target.getBoundingClientRect()
    setActiveNode(node.id)
    setDefinition(node.data.definition)
    setBubblePosition({ x: bRect.x + bRect.width + 10, y: bRect.y })
  }

  const controls = (node) => {
    const isExpanded = expanded.includes(node.id)
    const hasChildren = node.data.hasChildren

    return (
      <div className="flex flex-row p-1 gap-1 justify-end">
        <button
          className="rounded-sm w-6 text-green-500 hover:bg-green-200"
          onClick={(ev) => {
            include(node.id)
          }}
        >
          <PlusCircleIcon />
        </button>
        <button
          className="rounded-sm w-6 text-red-500 hover:bg-red-200"
          onClick={(ev) => {
            exclude(node.id)
          }}
        >
          <MinusCircleIcon />
        </button>
        {hasChildren ? (
          <button
            className="rounded-sm w-6 text-white place-self-end"
            onClick={(ev) => {
              isExpanded
                ? setExpanded(expanded.filter((id) => id !== node.id))
                : setExpanded([...expanded, node.id])
            }}
          >
            {isExpanded ? <ChevronDoubleLeftIcon /> : <ChevronDoubleRightIcon />}
          </button>
        ) : null}
      </div>
    )
  }
  return treeData ? (
    <>
      <div className="w-full h-full border-2 bg-gray-500 border-gray-900">
        <Canvas
          pannable={true}
          animated={false}
          nodes={treeData.nodes}
          edges={treeData.edges}
          node={(node: NodeProps) => (
            <Node onEnter={onNodeEnter} height={400}>
              {(event) => {
                return activeNode === event.node.id ? (
                  <foreignObject
                    className="overflow-visible z-10"
                    height={event.height}
                    width={event.width}
                    x={0}
                    y={0}
                  >
                    {controls(event.node)}
                  </foreignObject>
                ) : null
              }}
            </Node>
          )}
          direction="RIGHT"
        />
        {definition ? (
          <div
            className="absolute rounded-md p-2 bg-white w-96 italic text-gray-700"
            style={{ top: bubblePosition.y, left: bubblePosition.x }}
          >
            <span>{definition}</span>
          </div>
        ) : null}
      </div>
    </>
  ) : null
}
