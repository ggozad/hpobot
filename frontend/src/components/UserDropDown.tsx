import { LogoutIcon } from '@heroicons/react/solid'
import useAuth from 'auth'
import React from 'react'
import { useSelector } from 'react-redux'

// import { useNavigate } from 'react-router-dom'
import Menu from 'components/DropDownMenu'

import { selectCurrentUser } from 'store/user/usersSlice'

export default function UserDropDownMenu() {
  const userData = useSelector(selectCurrentUser)
  const { logout } = useAuth()
  // const navigate = useNavigate()

  return (
    <Menu title={userData?.fullname ?? 'Not logged in'}>
      {/* <Menu.MenuItem
        title="Profile"
        onClick={() => {
          navigate('/profile')
        }}
      /> */}
      <Menu.MenuItem
        title="Logout"
        onClick={() => {
          logout()
        }}
        LeftIcon={LogoutIcon}
      />
    </Menu>
  )
}
