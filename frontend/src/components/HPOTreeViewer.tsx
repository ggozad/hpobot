import { ChevronRightIcon, MinusCircleIcon, PlusCircleIcon } from '@heroicons/react/solid'
import API from 'API'
import React, { useEffect, useState } from 'react'

import useHPO from 'components/HPOContext'

import { HPOTreeNode } from 'types/hpo'

import { HPOToTree } from 'utils/hpo'

interface HPONodeProps {
  node: HPOTreeNode
  currentTermId: string
  onRootChange?: (node: string) => void
}

function HPONode({ node, currentTermId, onRootChange }: HPONodeProps) {
  const { include, exclude } = useHPO()
  const children = node.children || []
  const hasChildren = children.length > 0
  const isCurrent = node.id === currentTermId
  const showChildren =
    hasChildren &&
    (isCurrent ||
      (node.children &&
        node.children?.filter((child) => child.id === currentTermId).length > 0))

  const setNewRoot = (termId: string) => {
    if (onRootChange) {
      onRootChange(termId)
    }
  }

  const controls = (node) => {
    return (
      <div className="flex flex-row ml-2 gap-1 justify-end">
        <button
          className="rounded-sm w-6 text-green-500"
          onClick={(ev) => {
            include(node.id)
          }}
        >
          <PlusCircleIcon />
        </button>
        <button
          className="rounded-sm w-6 text-red-500"
          onClick={(ev) => {
            exclude(node.id)
          }}
        >
          <MinusCircleIcon />
        </button>
      </div>
    )
  }

  return (
    <>
      <div className="flex">
        <div className="place-self-center w-6">
          {hasChildren ? <ChevronRightIcon className="" /> : null}
        </div>
        <div
          className={`${isCurrent ? 'text-blue-700' : 'text-gray-500'}`}
          onClick={() => setNewRoot(node.id)}
        >
          {node.name}
        </div>
        {isCurrent ? controls(node) : null}
      </div>
      {showChildren ? (
        <div className="ml-5">
          {children.map((node) => (
            <HPONode
              key={node.id}
              node={node}
              currentTermId={currentTermId}
              onRootChange={onRootChange}
            />
          ))}
        </div>
      ) : null}
    </>
  )
}

interface HPOViewerProps {
  term: string
}

export default function HPOViewer({ term }: HPOViewerProps) {
  const [rootId, setRootId] = useState('')
  const [root, setRoot] = useState<HPOTreeNode | null>(null)

  useEffect(() => {
    setRootId(term)
  }, [term])

  useEffect(() => {
    API.getTerm(rootId).then((term) => {
      if (term) {
        setRoot(HPOToTree(term))
      }
    })
  }, [rootId])

  return root ? (
    <HPONode
      node={root}
      currentTermId={rootId}
      onRootChange={(rootId) => {
        setRootId(rootId)
      }}
    />
  ) : null
}
