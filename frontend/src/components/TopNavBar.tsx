import { Disclosure } from '@headlessui/react'
import { NavLink } from 'react-router-dom'

import UserDropDownMenu from 'components/UserDropDown'

import logo from 'resources/hpo-logo-white-no-words.png'

const navLinks = [
  {
    to: '/',
    title: 'Search',
  },
  {
    to: '/explore',
    title: 'Explore',
  },
]

export default function TopNavBar() {
  return (
    <Disclosure as="nav" className="bg-gray-800">
      {({ open }) => (
        <>
          <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="flex items-center justify-between h-16">
              <div className="flex items-center w-full">
                <div className="flex-shrink-0">
                  <img className="block h-8 w-auto" src={logo} alt="Workflow" />
                </div>
                <h3 className="block w-32 text-white font-bold text-xl ml-3">HPO bot</h3>
                <div className="ml-6 w-full">
                  <div className="flex space-x-4 flex-row">
                    {navLinks.map((link) => (
                      <NavLink
                        key={link.title}
                        to={link.to}
                        className={({ isActive }) =>
                          isActive
                            ? 'bg-gray-900 text-white px-3 py-2 rounded-md text-sm font-medium'
                            : 'text-gray-300 px-3 py-2 text-sm font-medium hover:bg-gray-700 hover:text-white'
                        }
                      >
                        {link.title}
                      </NavLink>
                    ))}
                    <div className="flex flex-grow"></div>
                    <div className="ml-6 h-8 justify-end flex ">
                      <UserDropDownMenu />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </Disclosure>
  )
}
