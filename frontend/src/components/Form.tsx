import React, { FormHTMLAttributes } from 'react'

import Error from 'components/Error'

interface Props extends FormHTMLAttributes<HTMLFormElement> {
  error?: string
}

export default function Form({ error, children, ...props }: Props) {
  return (
    <div className="flex flex-col w-full">
      {error && <Error error={error} />}
      <form {...props}>{children}</form>
    </div>
  )
}
