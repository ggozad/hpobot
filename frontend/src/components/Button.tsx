import React, { MouseEvent, ReactNode } from 'react'

interface Props {
  children: ReactNode
  onClick?: (event: MouseEvent<HTMLButtonElement>) => void
  type?: 'button' | 'submit' | 'reset'
  disabled?: boolean
}

export default function Button({
  children,
  onClick,
  type = 'button',
  disabled = false,
}: Props) {
  return (
    <button
      className={`w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium  focus:outline-none  whitespace-nowrap ${
        disabled
          ? 'bg-gray-300 cursor-default'
          : 'text-white bg-indigo-600 hover:bg-indigo-700 focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
      }`}
      onClick={(event) => {
        if (!disabled && onClick) {
          onClick(event)
        }
      }}
      type={type}
    >
      {children}
    </button>
  )
}
