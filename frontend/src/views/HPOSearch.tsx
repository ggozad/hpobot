import API from 'API'
import debounce from 'lodash/debounce'
import React, { useMemo, useState } from 'react'

import useHPO from 'components/HPOContext'
import HPOViewer from 'components/HPOTreeViewer'
import TextArea from 'components/TextArea'

export default function HPOExplorer() {
  const [searchText, setSearchText] = useState('')
  const [candidates, setCandidates] = useState<string[]>([])
  const [propabilities, setPropabilities] = useState<{ [key: string]: number }>({})
  const { included, excluded } = useHPO()

  const debouncedSearchPhenotype = useMemo(() => {
    return debounce(async (text) => {
      const probabilities = await API.searchPhenotype(text.trim())
      setPropabilities(probabilities)
      const candidates = Object.keys(probabilities)
      setCandidates(candidates)
    }, 1000)
  }, [])

  return (
    <div className="my-10 mx-10 h-full">
      <div className="grid grid-cols-2 gap-4 mt-10">
        <TextArea
          label="Clinical description"
          value={searchText}
          onChange={(ev) => {
            setSearchText(ev.target.value)
            debouncedSearchPhenotype(ev.target.value)
          }}
        />
        <div className="grid grid-cols-2 gap-4">
          <div>
            <label>Included</label>
            <ul>
              {[...included].map((term) => {
                return <li key={term}>{term}</li>
              })}
            </ul>
          </div>
          <div>
            <label>Excluded</label>
            <ul>
              {[...excluded].map((term) => {
                return <li key={term}>{term}</li>
              })}
            </ul>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-1 gap-4 mt-10">
        {candidates.map((candidate) => (
          <div key={candidate} className="mt-3 ">
            {propabilities[candidate] ? (
              <label className="font-semibold italic">
                Probability: {propabilities[candidate].toFixed(2)}
              </label>
            ) : null}
            <HPOViewer term={candidate} />
          </div>
        ))}
      </div>
    </div>
  )
}
