import useAuth from 'auth'
import React, { FormEvent, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import * as yup from 'yup'

import Button from 'components/Button'
import Form from 'components/Form'
import Input from 'components/Input'

import validateSchema from 'utils/forms'

const validationSchema = yup.object().shape({
  email: yup.string().label('email').required(),
  password: yup.string().label('Password').required(),
})

export default function Login() {
  const navigate = useNavigate()
  const { login } = useAuth()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [generalFormError, setGeneralFormError] = useState('')

  const [formFieldErrors, setFormFieldErrors] = useState<Record<string, string>>({
    email: '',
    password: '',
  })

  const validateForm = () => {
    const { valid, errors } = validateSchema(validationSchema, {
      email,
      password,
    })
    if (!valid) {
      setFormFieldErrors(errors)
    } else {
      setFormFieldErrors({ email: '', password: '' })
    }
    setGeneralFormError('')
    return valid
  }

  useEffect(() => {
    login().then(() => navigate('/'))
  }, [])

  async function handleSubmit(ev: FormEvent) {
    ev.preventDefault()
    const valid = validateForm()
    if (!valid) return
    try {
      await login(email, password)
      navigate('/')
    } catch (err: any) {
      if (err.response?.status === 401) {
        setGeneralFormError('Invalid email or password')
      } else {
        setGeneralFormError('A server error occurred')
      }
      console.error(err)
    }
  }

  return (
    <div className="min-h-full flex flex-col justify-center py-12 sm:px-6 lg:px-8">
      <div className="sm:mx-auto sm:w-full sm:max-w-md">
        <h2 className="text-center text-3xl font-extrabold text-gray-900">
          Sign in to your account
        </h2>
      </div>

      <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
          <Form className="space-y-6" error={generalFormError} onSubmit={handleSubmit}>
            <div className="flex rounded-md shadow-sm">
              <Input
                type="text"
                name="email"
                label="Email"
                value={email}
                onChange={(ev) => setEmail(ev.target.value)}
                error={formFieldErrors?.email}
              />
            </div>
            <div className="flex rounded-md shadow-sm">
              <Input
                type="password"
                name="password"
                label="Password"
                value={password}
                onChange={(ev) => setPassword(ev.target.value)}
                error={formFieldErrors?.password}
              />
            </div>
            <div>
              <Button type="submit">Sign in</Button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  )
}
