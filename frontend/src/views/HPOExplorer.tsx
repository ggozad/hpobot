import React, { useState } from 'react'

import Button from 'components/Button'
import useHPO from 'components/HPOContext'
import HPOViewer from 'components/HPOTreeViewer'
import Input from 'components/Input'

export default function HPOExplorer() {
  const [searchText, setSearchText] = useState('HP:0000100')
  const [term, setTerm] = useState('')

  const { included, excluded } = useHPO()

  const onSubmit = async (e) => {
    e.preventDefault()
    setTerm(searchText)
  }

  return (
    <div className="my-10 mx-10 h-full">
      <div className="grid grid-cols-2 gap-4 mt-10">
        <form onSubmit={onSubmit}>
          <Input
            label="HPO term"
            value={searchText}
            onChange={(ev) => setSearchText(ev.target.value)}
          />
          <div className="mt-2 w-1/4 flex items-center">
            <Button type="submit">Explore</Button>
          </div>
        </form>
        <div className="grid grid-cols-2 gap-4">
          <div>
            <label>Included</label>
            <ul>
              {included &&
                [...included].map((term) => {
                  return <li key={term}>{term}</li>
                })}
            </ul>
          </div>
          <div>
            <label>Excluded</label>
            <ul>
              {excluded &&
                [...excluded].map((term) => {
                  return <li key={term}>{term}</li>
                })}
            </ul>
          </div>
        </div>
      </div>
      <div className="w-full h-4/6 mt-10">
        <HPOViewer term={term} />
      </div>
    </div>
  )
}
