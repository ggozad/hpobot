# Resources

HPO terms:
https://hpo.jax.org/app/download/ontology

# Local HPOBot instance

## Prerequisites

You need to have installed:

- Docker (for example via [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac/))
- mkcert to cresate a self-signed certificate.
  Instructions can be found here for [linux](https://github.com/FiloSottile/mkcert#linux), [mac](https://github.com/FiloSottile/mkcert#macos) and [windows](https://github.com/FiloSottile/mkcert#windows).

After you install mkcert, make sure you run it with the `--install` flag.

## One-time initialisation

First you need to generate some passwords for your environment:

```shell
./scripts/gen-secrets > .env
```

And generate a SSL certificate:

```shell
cd secrets
mkcert \*.local.hpobot.dev
```

Edit `/etc/hosts` to include the following lines:

```text
127.0.0.1 api.local.hpobot.dev app.local.hpobot.dev
```

Finally create the database:

```shell
docker-compose run backend alembic upgrade head
```

## Starting hpobot

You can now run `docker-compose`:

```shell
$ docker-compose up
...
```

The first time you run `docker-compose` you will have to wait quite a bit until all language models are computed and saved. Once this is complete you can use the web frontend.

```
https://app.local.hpobot.dev:8080
```

## Development

You can generate alembic migrations when you update the database schema:

```
docker-compose run backend alembic revision --autogenerate
```
